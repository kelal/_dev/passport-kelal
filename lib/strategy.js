// load modules
var OAuth2Strategy = require('passport-oauth2')
  , util = require('util')
  , ProfileParser = require('./profile-parser')
  , InternalOAuthError = require('passport-oauth2').InternalOAuthError

/**
 * `Strategy` constructor.
 *
 * The Kelal authentication strategy authenticates requests by delegating to
 * Kelal Profile using the OAuth 2.0 protocol.
 *
 * Applications must supply a `verify` callback which accepts an `accessToken`,
 * `refreshToken` and service-specific `profile`, and then calls the `cb`
 * callback supplying a `user`, which should be set to `false` if the
 * credentials are not valid.  If an exception occurred, `err` should be set.
 *
 * Options:
 *   - `appId`        your Kelal-linked application's client id
 *   - `appSecret`    your Kelal-linked application's client secret
 *   - `callbackUrl`  URL to which Kelal Profile will redirect the user after granting authorization
 *
 * Examples:
 *
 *     passport.use(new KelalStrategy({
 *         appId: '0123456789abcdef',
 *         appSecret: 'shhh-its-a-secret'
 *         callbackUrl: 'https://www.example.net/auth/kelal/callback'
 *       },
 *       function(accessToken, refreshToken, profile, cb) {
 *         User.findOrCreate(..., function (err, user) {
 *           cb(err, user)
 *         })
 *       }
 *     ))
 *
 * @constructor
 * @param {object} options
 * @param {function} verify
 * @access public
 */
function Strategy(options, verify) {
  options = options || {}

  options.clientID = options.appId || options.appID || options.clientID || options.clientId
  options.clientSecret = options.appSecret || options.clientSecret
  options.callbackURL = options.callbackUrl || options.callbackURL || options.callbackUri || options.callbackURI || options.redirectURI || options.redirectUri || options.redirectURL || options.redirectUrl || options.redirect_uri || options.redirect_url

  options.authorizationURL = options.authorizationURL || 'https://kelalprofile.herokuapp.com/app/connect'
  options.tokenURL = options.tokenURL || 'https://kelalprofile.herokuapp.com/api/oauth2/token'

  OAuth2Strategy.call(this, options, verify)
  this.name = 'kelal'
  this._userProfileURL = options.userProfileURL || 'https://kelalprofile.herokuapp.com/api/profile/me'
}

// inherit from `OAuth2Strategy`
util.inherits(Strategy, OAuth2Strategy)

/**
 * Retrieve user profile from Google.
 *
 * This function constructs a normalized profile, with the following properties:
 *
 *   - `provider`         always set to `kelal`
 *   - `id`
 *   - `username`
 *   - `displayName`
 *
 * @param {string} accessToken
 * @param {function} done
 * @access protected
 */
Strategy.prototype.userProfile = function(accessToken, done) {
  this._oauth2.get(this._userProfileURL, accessToken, function (err, body, res) {
    var json

    if (err) {
      if (err.data) {
        try {
          json = JSON.parse(err.data)
        } catch (_) {}
      }
      return done(new InternalOAuthError('Failed to retrieve profile', err))
    }

    try {
      json = JSON.parse(body)
    } catch (ex) {
      return done(new Error('Failed to parse response body'))
    }

    if (!json || json.success !== true || !json.profile) {
      return done(new Error(json.problem || 'Failed to parse profile from json response body'))
    }

    var profile = ProfileParser.parse(json.profile)
    profile.provider  = 'kelal'
    profile._raw = body
    profile._json = json

    done(null, profile)
  })
}

/**
 * Expose `Strategy`.
 */
module.exports = Strategy
