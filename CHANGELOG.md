# passport-kelal changelog

## v0.3.0

- Changed Kelal Profile OAuth 2.0 domain from http://localhost:1414 to https://kelalprofile.herokuapp.com

## v0.2.4

- Fix profile response

## v0.2.3

- Improved aliasing of config keys

## v0.2.2

- Update protocol

## v0.2.1

- Fix README

## v0.2.0

- Add `appId` and `appSecret` and other aliases

## v0.1.1

- Add changelog
- Better error management

## v0.1.0

- Full-featured initial commits

## v0.0.0

- Initiate project