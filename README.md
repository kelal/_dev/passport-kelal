# passport-kelal

[Passport](http://passportjs.org/) strategy for authenticating with [Kelal Profile](https://gitlab.com/kelal/platform/profile)
using the OAuth 2.0 API.

This module lets you authenticate using Kelal Profile in your Node.js applications.
By plugging into Passport, Kelal Profile authentication can be easily and
unobtrusively integrated into any application or framework that supports
[Connect](http://www.senchalabs.org/connect/)-style middleware, including
[Express](http://expressjs.com/).

## Install

```bash
$ npm install passport-kelal
```

## Usage

#### Create an Application

Before using `passport-kelal`, you must register your application with The
Kelal Platform.  If you have not already done so, a new project can be created in the
TODO (COMING SOON).
Your application will be issued a app ID and app secret, which need to be
provided to the strategy.  You will also need to configure a redirect URI which
matches the route in your registered application.

#### Configure Strategy

The Kelal authentication strategy authenticates users using a Kelal profile
and OAuth 2.0 tokens.  The app ID and secret obtained when creating an
application are supplied as options when creating the strategy.  The strategy
also requires a `verify` callback, which receives the access token and optional
refresh token, as well as `profile` which contains the authenticated user's
Kelal Platform profile.  The `verify` callback must call `cb` providing a user to
complete authentication.

```javascript
var KelalStrategy = require('passport-kelal').Strategy

passport.use(new KelalStrategy({
    appID: APP_ID,
    appSecret: APP_SECRET,
    callbackUrl: "http://www.example.com/auth/kelal/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ kelalProfileId: profile.id }, function (err, user) {
      return cb(err, user)
    })
  }
))
```

#### Authenticate Requests

Use `passport.authenticate()`, specifying the `'kelal'` strategy, to
authenticate requests.

For example, as route middleware in an [Express](http://expressjs.com/)
application:

```javascript
app.get('/auth/kelal',
  passport.authenticate('kelal', { scope: ['profile'] }))

app.get('/auth/kelal/callback', 
  passport.authenticate('kelal', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/')
  })
```

## Examples

Developers using the popular [Express](http://expressjs.com/) web framework can
refer to an [example](https://github.com/passport/express-4.x-facebook-example)
as a starting point for their own web applications.  The example shows how to
authenticate users using Facebook.  However, because both Facebook and Kelal Profile
use OAuth 2.0, the code is similar.  Simply replace references to Facebook with
corresponding references to Kelal.

## License

Forked from [GitHub: jaredhanson/passport-google-oauth2](https://github.com/jaredhanson/passport-google-oauth2).

[The MIT License](http://opensource.org/licenses/MIT)

Copyright (c) 2017 Kelal / Kelal Development Tools
